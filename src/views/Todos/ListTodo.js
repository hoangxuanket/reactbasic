import React, { Component } from 'react'

export default class List extends Component {

    handleDeleteTodo = (todo) => {
        this.props.handleDeleteTodo(todo)
    }

    handleEditTodo = (todo) => {
        this.props.handleEditTodo(todo)
    }

    handleOnchangeEditTodo = (todo) => {
        this.props.handleOnchangeEditTodo(todo)
    }

    render() {
        console.log(this.props);
        let { listTodos, editTodo } = this.props.data;
        let isEmptyObj = Object.keys(editTodo).length === 0;
        return (
            <div className="list-todo-content">
                {listTodos && listTodos.length > 0 && listTodos.map((item, index) => {
                    return (
                        <div className="todo-child" key={item.id}>
                            {isEmptyObj === true ?
                                <span> {index + 1} - {item.title} </span>
                                :
                                <>
                                    {editTodo.id === item.id ?
                                        <span>
                                            {index + 1} - <input value={editTodo.title}
                                                onChange={(event) => this.handleOnchangeEditTodo(event)} />
                                        </span>
                                        :
                                        <span>
                                            {index + 1} - {item.title}
                                        </span>
                                    }
                                </>
                            }
                            <button className="edit"
                                onClick={() => this.handleEditTodo(item)}
                            >
                                {isEmptyObj === false && editTodo.id === item.id ?
                                    'Save' : 'Edit'
                                }
                            </button>
                            <button className="delete"
                                onClick={() => this.handleDeleteTodo(item)}
                            >Delete
                            </button>
                        </div>
                    )
                })}

            </div>
        )
    }
}
