import React from "react";
import './ListTodo.scss'

import AddTodo from "./AddTodo";
import ListTodo from "./ListTodo";

import { toast } from 'react-toastify';
// import Color from "../HOC/Color";

class TodoComponent extends React.Component {

    state = {
        listTodos: [
            {
                id: 'todo1',
                title: 'Doing homework'
            },
            {
                id: 'todo2',
                title: 'Making video'
            },
            {
                id: 'todo3',
                title: 'Fixing bugs'
            }
        ],
        editTodo: {}
    }

    addNewTodo = (todo) => {
        this.setState({
            listTodos: [...this.state.listTodos, todo]
        })

        toast.success('Add success!')
    }

    handleDeleteTodo = (todo) => {
        let currentTodos = this.state.listTodos;
        currentTodos = currentTodos.filter(item => item.id !== todo.id)
        this.setState({
            listTodos: currentTodos
        })
        toast.success('Delete success!')
    }

    handleEditTodo = (todo) => {
        let { editTodo, listTodos } = this.state
        let isEmptyObj = Object.keys(editTodo).length === 0;

        //save
        if (isEmptyObj === false && editTodo.id === todo.id) {

            let listTodosCopy = [...listTodos];

            let objIndex = listTodosCopy.findIndex((item => item.id === todo.id));
            listTodosCopy[objIndex].title = editTodo.title

            this.setState({
                listTodos: listTodosCopy,
                editTodo: {}
            })
            toast.success('Update todo success!')
            return;
        }

        //edit
        this.setState({
            editTodo: todo
        })

    }

    handleOnchangeEditTodo = (event) => {
        let editTodoCopy = { ...this.state.editTodo }
        editTodoCopy.title = event.target.value
        this.setState({
            editTodo: editTodoCopy
        })
    }

    render() {
        return (
            <>
                <div style={{ fontSize: "30px", marginBottom: "20px" }}>Todo App</div>
                <div className="list-todo-container">

                    <AddTodo
                        addNewTodo={this.addNewTodo}
                    />

                    <ListTodo
                        data={this.state}
                        handleOnchangeEditTodo={this.handleOnchangeEditTodo}
                        handleEditTodo={this.handleEditTodo}
                        handleDeleteTodo={this.handleDeleteTodo}

                    />
                </div>
            </>
        )
    }
}

export default TodoComponent;