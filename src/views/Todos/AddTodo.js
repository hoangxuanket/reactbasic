import React from "react";
import { toast } from "react-toastify";
// import { Placeholder } from "react-bootstrap";

class AddTodo extends React.Component {
  state = {
    title: "",
  };

  handleOnchange = (event) => {
    this.setState({
      title: event.target.value,
    });
  };

  handleAddTodo = (event) => {
    var date = new Date();
    event.preventDefault(); // khong tai lai website
    if (!this.state.title) {
      toast.error(" Missing title's todo!");
      return;
    }
    let todo = {
      // id: Math.floor(Math.random() * 100),
      id: date.getTime(),
      title: this.state.title,
    };

    this.props.addNewTodo(todo);

    this.setState({
      title: "",
    });
  };

  render() {
    let { title } = this.state;
    return (
      <div className="add-todo">
        <input
          type="text"
          value={title}
          onChange={(event) => this.handleOnchange(event)}
          placeholder="Enter title"
          required
        />
        <button
          type="button"
          className="add"
          id="add"
          onClick={(event) => this.handleAddTodo(event)}
        >
          Add
        </button>
      </div>
    );
  }
}

export default AddTodo;
