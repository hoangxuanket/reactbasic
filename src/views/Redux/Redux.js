/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from "react";
// import { withRouter } from "react-router";
// import Color from "../HOC/Color";
import logo from "../../assets/images/logo.png";
import ReactDOM from "react-dom";
import { connect } from "react-redux";

class Redux extends Component {

  handleDeleteUser = (user) => {
    this.props.deleteUserRedux(user)
  };

  handleCreateUser = () => {
    this.props.addUserRedux()
  }

  render() {
    let listUsers = this.props.dataRedux
    return (
      <>
        <h2> Homepage React </h2>
        <div>{listUsers && listUsers.length > 0 &&
          listUsers.map((item, index) => {
            return (
              <div key={item.id}>
                {index + 1} - {item.name}
                &nbsp; <span onClick={() => this.handleDeleteUser(item)}> x</span>
              </div>
            )
          })
        }
          <button onClick={() => this.handleCreateUser()}>Add user</button>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dataRedux: state.users
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteUserRedux: (userDelete) => dispatch({ type: 'DELETE_USER', payload: userDelete }),
    addUserRedux: () => dispatch({ type: 'CREATE_USER' }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Redux);

