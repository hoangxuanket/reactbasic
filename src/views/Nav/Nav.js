import React from "react";
import "./Nav.scss";
import {
    NavLink
} from "react-router-dom";

class Nav extends React.Component {
    render() {
        return (
            <div className="topnav">
                <NavLink to='/' activeClassName="active" exact={true}>Home</NavLink>
                <NavLink to='/componentApi' activeClassName="active">Component API</NavLink>
                <NavLink to='/lifeCycle' activeClassName="active">Life Cycle</NavLink>
                <NavLink to='/handleEvent' activeClassName="active">Handle Event</NavLink>
                <NavLink to='/form' activeClassName="active">Form</NavLink>
                <NavLink to='/list' activeClassName="active">List</NavLink>
                <NavLink to='/todo' activeClassName="active">Todos</NavLink>
                <NavLink to='/about' activeClassName="active">About</NavLink>
                <NavLink to='/hook' activeClassName="active">Hooks</NavLink>
            </div>
        )
    }
}

export default Nav;