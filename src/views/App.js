import logo from './logo.svg';
import './App.scss';

import Home from './Home/Home';
import MyComponent from './About/MyComponent';
import TodoComponent from './Todos/TodoComponent';
import List from './List/List';
import LifeComponent from './ComponentLife/LifeComponent';
import HandleEvent from './HandleEvent/HandleEvent'
import HookComponent from './Hook/HookComponent';
import ComponetAPI from './Component API/ComponentAPI';
import Form from './Form/Form';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Nav from './Nav/Nav';
import {
  BrowserRouter,
  Switch,
  Route
} from "react-router-dom";

/**
* 2. components: class component / function component (function, arrow)
* function component không sử dụng hook thì chỉ có 1 nhiệm vụ thằng cha nhận đầu vào ntn thì render đúng như vậy, nó không phải xử lý logic gì cả
* class component xử lý logic hander state, prop
*/

// const App = () => {}
function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <header className="App-header">
          <Nav />
          <img src={logo} className="App-logo" alt="logo" />

          <Switch >
            <Route exact path="/" >
              <Home />
            </Route>
            <Route path="/componentApi" >
              <ComponetAPI />
            </Route>
            <Route path="/lifeCycle" >
              <LifeComponent />
            </Route>
            <Route path="/handleEvent" >
              <HandleEvent />
            </Route>
            <Route path="/form" >
              <Form />
            </Route>
            <Route path="/todo" >
              <TodoComponent />
            </Route>
            <Route path="/about" >
              <MyComponent />
            </Route>
            <Route path="/list">
              <List />
            </Route>
            <Route path="/hook" >
              <HookComponent />
            </Route>
          </Switch>

        </header>

        <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
        />
        {/* Same as */}
        <ToastContainer />
      </div>
    </BrowserRouter>

  );
}

export default App;
