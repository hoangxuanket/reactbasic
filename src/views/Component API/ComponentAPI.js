/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from "react";
import ReactDOM from "react-dom";

class ComponetAPI extends Component {

  // state declaration
  state = {
    message: "Hello World",
  };

  // update state (component API setState)
  updateSetState = () => {
    this.setState({
      message: "It is a beautiful day.",
    });
  };

  // component API forceUpdate 
  handleForceUpdate = () => {
    this.forceUpdate();
  };

  //component API reactDOM.findDomnode
  handleFindDomNode = () => {
    var myDivOne = document.getElementById("myDivOne");

    //change color
    ReactDOM.findDOMNode(myDivOne).style.color = "#FF8C00";

    //change background
    ReactDOM.findDOMNode(myDivOne).style.background = "#008000";
  };


  render() {
    let [message] = this.state.message
    return (
      <>

        {/* setState */}
        <div style={{ marginBottom: "20px" }}>
          <h4>Update setState</h4>
          <button onClick={() => this.updateSetState()}>Update</button>
          <h4>{this.state.message}</h4>
        </div>

        {/* handleForceUpdate */}
        <div>
          <h4>Handle Force Update</h4>
          <button type="button" onClick={() => this.handleForceUpdate()}>
            Random
          </button>
          <p> Random number: {Math.random()} </p>
        </div>

        {/* findDomNode */}
        <div style={{ marginBottom: "20px" }}>
          <h4>ReactJS Find DOM Node</h4>
          <button onClick={() => this.handleFindDomNode()}>
            Find_Dom_Node
          </button>
          <h4 id="myDivOne">NODE</h4>
        </div>
      </>
    );
  }
}

export default ComponetAPI;