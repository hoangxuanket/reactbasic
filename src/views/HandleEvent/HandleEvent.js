/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from "react";

class HandleEvent extends Component {

    state = {
        isToggleOn: true,
    };

    shoot = (a) => {
        alert(a);
    }

    // event change toggle
    handleClick = () => {
        this.setState((prevState) => ({
            isToggleOn: !prevState.isToggleOn,
        }));
    };

    render() {
        return (
            <>
                {/* pass arguments handle event */}
                <h4> Pass arguments handle event </h4>
                <button style={{ marginBottom: "20px" }}
                    onClick={() => this.shoot("Goal!")}>
                    Take the shot!
                </button>

                {/* toggle on and off */}
                <div style={{ marginBottom: "10px" }}>
                    <h4>Togged On or Off</h4>
                    <button onClick={this.handleClick}>
                        {this.state.isToggleOn ? "ON" : "OFF"}
                    </button>
                </div>
            </>
        );
    }
}

export default HandleEvent;