import React from "react";
import axios from "axios";
import { withRouter } from "react-router";

import "./List.scss";

class ListUser extends React.Component {
  state = {
    number: [1, 2, 3, 4, 5],
  };


  render() {
    let { number } = this.state;

    const temp = []
    number.forEach((number) => {
      temp.push(<li>{number}</li>)
    })
    return (
      <div className="list-user-container">
        <div>
          {temp}
        </div>
      </div>
    );
  }
}

export default withRouter(ListUser);
