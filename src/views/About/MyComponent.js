import React from "react";
import ChildComponent from "./ChildComponent";
import AddComponent from "./AddComponent";

class MyComponent extends React.Component {

    state = {
        arrJobs: [
            {
                id: '1',
                title: 'Developers',
                salary: '500'
            },
            {
                id: '2',
                title: 'Testers',
                salary: '400'
            },
            {
                id: '3',
                title: 'Managers',
                salary: '1000'
            }
        ]
    }

    addNewJob = (job) => {
        // let currentJobs = this.state.arrJobs;
        // currentJobs.push(job)
        this.setState({
            arrJobs: [...this.state.arrJobs, job]
            // arrJobs: currentJobs
        })
    }

    deleteAJob = (job) => {
        let currentJobs = this.state.arrJobs;
        currentJobs = currentJobs.filter(item => item.id !== job.id);
        this.setState({
            arrJobs: currentJobs
        })
    }

    // componentDidUpdate(presProps, prevState) {
    //     console.log('>> run did update: ', 'prev state', prevState, ' current state: ', this.state);
    // }

    // componentDidMount() {
    //     console.log(">>> run component did mount")
    // }

    render() {

        return (
            <>
                <AddComponent
                    addNewJob={this.addNewJob}
                />
                <ChildComponent
                    arrJobs={this.state.arrJobs}
                    deleteAJob={this.deleteAJob}
                />
            </>
        )
    }

}

export default MyComponent; // export 1 class