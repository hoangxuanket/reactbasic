import React from "react";
import { toast } from 'react-toastify';

class AddComponent extends React.Component {

    state = {
        title: '',
        salary: ''
    }

    // xử lý nhiều đầu vào
    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    // handleChangeTitleJob = (event) => {
    //     this.setState({
    //         title: event.target.value
    //     })
    // }

    // handleChangeSalary = (event) => {
    //     this.setState({
    //         salary: event.target.value
    //     })
    // }

    handleSubmit = (event) => {
        event.preventDefault() // khong tai lai website
        if (!this.state.title) {
            toast.error(" Missing title's todo!")
            return
        } else if (!this.state.salary) {
            toast.error(" Missing salary todo!")
            return
        }
        this.props.addNewJob({
            id: Math.floor(Math.random() * 100),
            title: this.state.title,
            salary: this.state.salary
        })

        this.setState({
            title: '',
            salary: ''
        })
    }

    render() {
        return (
            <form>
                <label htmlFor="fname">Job's title:</label><br />
                <input
                    type="text"
                    name="title"
                    value={this.state.title}
                    onChange={(event) => this.handleChange(event)}
                />
                <br />
                <label htmlFor="lname">Salary:</label><br />
                <input
                    type="text"
                    name="salary"
                    value={this.state.salary}
                    onChange={(event) => this.handleChange(event)}
                />
                <br /><br />
                <input type="submit" value="Submit"
                    onClick={(event) => this.handleSubmit(event)}
                />
            </form>
        )
    }
}

export default AddComponent