import React from "react";
import { toast } from 'react-toastify';

class LifeComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: 0,
            show: true
        }
    };

    //Mounting
    componentDidMount() {
        console.log('component did mount');
    };

    increament() {
        this.setState({ data: this.state.data + 1 });
    }

    delHeader = () => {
        this.setState({ show: false });
    }

    render() {
        console.log('render call')
        return (
            <div className="container">
                <button onClick={() => this.increament()}>Increament</button>
                <button type="button" onClick={this.delHeader}>Delete Header</button>
                {this.state.show ? <Result myNumber={this.state.data} /> : <div></div>}
            </div>
        )
    }
}

class Result extends React.Component {

    //Updating
    shouldComponentUpdate() {
        console.log('shouldComponentUpdate called');
        return true
    }

    //Updating
    componentDidUpdate(prevProps, prevState) {
        console.log('componentDidUpdate called');
    }

    //Unmounting
    componentWillUnmount(prevProps, prevState) {
        console.log('component will unmount')
    }

    render() {
        return (
            <div>
                <h3>{this.props.myNumber}</h3>
            </div>
        )
    }
}

export default LifeComponent
