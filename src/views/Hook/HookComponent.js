import React, { useEffect, useState } from 'react'

export default function ExampleComponen() {

    const [count, setCount] = useState(0);
    const [age, setAge] = useState(42);
    const [fruit, setFruit] = useState('banana');
    const [todos, setTodos] = useState([{ text: 'Learn Hooks' }]);

    useEffect(() => {
        document.title = `You clicked ${count} times`;
    })
    return (
        <div>
            <div>{count}</div>
            <button onClick={() => setCount(count + 1)} style={{ marginBottom: "20px" }}>
                Click me
            </button>
            <TodoList />
        </div>

    )
}

const TodoList = () => {
    const [name, setName] = useState("")
    return (
        <div>
            <input type='text' onChange={(event) => setName(event.target.value)} />
            <br /><br />
            Hello name = {name}
        </div>
    )
}


