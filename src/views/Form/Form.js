/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from "react";

class Form extends Component {

    state = {
        message: "",
        // detail: "",
    };

    // event change multiple input
    handleChange = (event) => {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({
            [name]: value,
        });
    };

    // submit
    handleSubmit = (event) => {
        alert("form: " + this.state.message + ", " + this.state.detail);
        event.preventDefault();
    };

    render() {
        return (
            <>
                <h2> Form React </h2>

                {/* form */}
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <h4>Form</h4>
                        <label>Title:</label>
                        <br />
                        <input
                            type="text"
                            name="message"
                            value={this.state.message}
                            onChange={this.handleChange}
                        />
                        <br />
                        <label>Essay:</label>
                        <br />
                        <textarea
                            name="detail"
                            value={this.state.detail}
                            onChange={this.handleChange}
                        />
                        <br /> <br />
                        <input type="submit" value="Submit" />
                    </form>
                </div>
            </>
        );
    }
}

export default Form;